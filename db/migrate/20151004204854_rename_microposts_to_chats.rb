class RenameMicropostsToChats< ActiveRecord::Migration
	def change
		rename_table :microposts, :chats
	end
end