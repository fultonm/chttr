class ApiController < ApplicationController
	skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	before_action :signed_in_user, only: [:current_user, :relationships, :chats]
	def user
		render :json => current_user
	end
	def relationships
		@user = current_user
		render :json => {
			:followers => @user.followers,
			:following => @user.following
		}
	end
	def chats
		data_hash = JSON.parse(params["_json"].to_s)
		@user = User.find_by(id: data_hash["UserId"])
		render :json => @user.chats
	end
	def feed
		render :json => current_user.feed.paginate(page: 1)
	end
	def submit_chat
		data_hash = JSON.parse(params["_json"].to_s)
		chat = Hash.new
		chat['content'] = data_hash["Content"]
		params[:chat] = chat
		@chat = current_user.chats.build(chat_params)
		if @chat.save
			render :json => { :success => "true" }
		else
			render :json => { :success => "false" } #stabby lambda xD
		end
	end
	private
		def chat_params			
			params.require(:chat).permit(:content, :picture)
		end
end