class UsersController < ApplicationController
	before_action :signed_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
	before_action :correct_user, only: [:edit, :update]
	before_action :admin_user, only: :destroy
  def index
    @users = User.where(activated: true).paginate(page: params[:page])
  end
	def show
		@user = User.find(params[:id])
		redirect_to root_path and return unless @user.activated
		@chats = @user.chats.paginate(page: params[:page])
	end
 	def new
 		@user = User.new
 	end
	def create
		@user = User.new(user_params) # NOT THE FINAL IMPLEMATIOZNSZOR
		if @user.save
			@user.send_activation_email
			flash[:info] = "Please check your email at " + @user.email + " to activate your account"
			redirect_to root_path
		else
			render 'new'
		end
	end
	def edit
		@user = User.find(params[:id])
	end
	def update
		@user = User.find(params[:id])
		if @user.update_attributes(user_params)
			flash[:success] = "Details updated"
			redirect_to @user
		else
			render 'edit'
		end
	end
	def destroy
		@user = User.find(params[:id])
		name = @user.name
		@user.destroy
		flash[:success] = name + " deleted"
		redirect_to users_path
	end
	def following
		@title = "Following"
		@user = User.find(params[:id])
		@users = @user.following.paginate(page: params[:page])
		render 'show_follow'
	end
	def followers
		@title = "Followers"
		@user = User.find(params[:id])
		@users = @user.followers.paginate(page: params[:page])
		render 'show_follow'
	end
	private
		def user_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation)
		end
		def correct_user
			@user = User.find(params[:id])
			redirect_to(root_path) unless current_user?(@user)
		end
		def admin_user
			redirect_to(root_path) unless current_user.admin?
		end
end