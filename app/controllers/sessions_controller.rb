class SessionsController < ApplicationController
	skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	before_action :parse_json_request, :if => Proc.new { |c| c.request.format == 'application/json' }
  def new
  end
  def create
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
      if user.activated?
  		  sign_in user
     		params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        respond_to do |format|
          format.json { render :json => current_user }
          format.html { redirect_back_or user }
        end
  		else
    		message = "Account not activated. "
    		message += "Please check your email for an account activation link"
    		flash[:warning] = message
        respond_to do |format|
          format.json { render :json => message }
          format.html { redirect_to sign_in_path }
        end    		
			end
		else
      message = "Invalid email or password!"
			flash.now[:danger] = message
      respond_to do |format|
        format.json { render :json => message }
        format.html { render 'new' }
      end  
		end
	end
  def destroy
    sign_out if signed_in?
    redirect_to root_path
  end
  private
  	def parse_json_request
  		data_hash = JSON.parse(params["_json"].to_s)
  		params[:session][:email] = data_hash["Email"]
  		params[:session][:password] = data_hash["Password"]
  	end
end