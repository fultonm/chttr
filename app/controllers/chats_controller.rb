class ChatsController < ApplicationController
	before_action :signed_in_user, only: [:create, :destroy]
	before_action :correct_user, only: [:destroy]
	def create
		@chat = current_user.chats.build(chat_params)
		if @chat.save
			flash[:success] = "Created post!"
			redirect_to root_path
		else
			@feed_items = []
			render 'static_pages/home'
		end
	end
	def destroy
		@chat.destroy
		flash[:success] = "Post deleted"
		redirect_to request.referrer || root_path
	end
	private
		def chat_params
			params.require(:chat).permit(:content, :picture)
		end
		def correct_user
			@chat = current_user.chats.find_by(id: params[:id])
			redirect_to root_path if @chat.nil?
		end
end
