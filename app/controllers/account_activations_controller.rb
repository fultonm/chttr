class AccountActivationsController < ApplicationController
	def edit
		user = User.find_by(email: params[:email])
		if user && !user.activated? && user.authenticated?(:activation, params[:id])
			user.activate
			sign_in user
			flash[:success] = "Your account was successfully activate and you are signed in"
			redirect_to user
		else
			flash[:danger] = "Invalid activation link or you have already activated your account"
			redirect_to sign_in_path
		end
	end
end
