class Chat < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 320 }
  validate :picture_size
  private
  	def picture_size
  		if picture.size > 5.megabytes
  			errors.add(:picture, "cannot exceed 5 megabytes")
  		end
  	end
end
