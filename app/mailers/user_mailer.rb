class UserMailer < ApplicationMailer
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Activate your chttr account"
  end
  def password_reset(user)
    @user = user
    mail to: user.email, subject: "chttr password reset instructions"
  end
end