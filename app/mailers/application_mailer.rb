class ApplicationMailer < ActionMailer::Base
  default from: "noreply@wartimestudios.com"
  layout 'mailer'
end
