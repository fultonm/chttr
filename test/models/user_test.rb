require 'test_helper'
class UserTest < ActiveSupport::TestCase
	def setup
		@user = User.new(name: "Sylvia Chen", 
						 email: "sylviahc8080@gmail.com",
						 password: "foobar", 
						 password_confirmation: "foobar")
	end
	test "user should be valid" do
		assert @user.valid?
	end
	test "user name should be present" do
		@user.name = "       "
		assert_not @user.valid?
	end
	test "user email should be present" do
		@user.email = "    "
		assert_not @user.valid?
	end
	test "user name should not be too long" do
		@user.name = "a" * 51
		assert_not @user.valid?
	end
	test "user email should not be too long" do
		@user.email = "a" * 244 + "@example.com"
		assert_not @user.valid?
	end
	test "user email validation should accept valid email addresses" do
		valid_addresses = %w[foo_bar-baz@exampl.COM A_US-ciTiz3n@foo.bar.baz.co.uk first.last@foo.jp alice+bob@baz.tr]
		valid_addresses.each do |valid_address|
			@user.email = valid_address
			assert @user.valid?, "#{valid_address.inspect} should be valid"
		end
	end
	test "user email validation should reject invalid email addresses" do
		invalid_addresses = %w[user@example,com user_at_foo.org user@example@baz.org 49]
		invalid_addresses.each do |invalid_address|
			@user.email = invalid_address
			assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
		end
	end
	test "user email address should be unique" do
		duplicate_user = @user.dup
		duplicate_user.email = @user.email.upcase
		@user.save
		assert_not duplicate_user.valid?
	end
	test "user password should be present (nonblank)" do
		@user.password = @user.password_confirmation = " " * 6
		assert_not @user.valid?
	end
	test "user password should have a minimum length" do
		@user.password = @user.password_confirmation = "a" * 5
		assert_not @user.valid?
	end
	test "user with nil digest authenticated? should return false" do
		assert_not @user.authenticated?(:remember, '')
	end
	test "chats associated with destroyed user should be destroyed too" do
		@user.save
		@user.chats.create!(content: "Stabby lambda cmtfu")
		assert_difference 'Chat.count', -1 do
			@user.destroy
		end
	end
	test "following a user should change status to followed" do
		michael = users(:michael)
		archer = users(:archer)
		assert_not michael.following?(archer)
		michael.follow(archer)
		assert michael.following?(archer)
		assert archer.followers.include?(michael)
		michael.unfollow(archer)
		assert_not michael.following?(archer)
	end
	test "feed should have the correct chats" do
		michael = users(:michael)
		archer = users(:archer)
		lana = users(:lana)
		lana.chats.each do |chat_following|
			assert michael.feed.include?(chat_following)
		end
		michael.chats.each do |chat_self|
			assert michael.feed.include?(chat_self)
		end
		archer.chats.each do |chat_unfollowed|
			assert_not michael.feed.include?(chat_unfollowed)
		end
	end			
end