require 'test_helper'
class RelationshipTest < ActiveSupport::TestCase
	def setup
		@relationship = Relationship.new(follower_id: 1, followed_id: 2)
	end
	test "model relationship should be valid" do
		assert @relationship.valid?	
	end
	test "nil follower_id should be invalid" do
		@relationship.follower_id = nil
		assert_not @relationship.valid?
	end
	test "nil followed_id should be invalid" do
		@relationship.followed_id = nil
		assert_not @relationship.valid?
	end
end