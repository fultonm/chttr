require 'test_helper'
class ChatTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		# This code is not the ruby way
		@chat = @user.chats.build(content: "Lorem Ipsum")
	end
	test "model chat should be valid" do
		assert @chat.valid?
	end
	test "absence of user id should invalidate chat" do
		@chat.user_id = nil
		assert_not @chat.valid?
	end
	test "absence of content should invalidate chat" do
		@chat.content = nil
		assert_not @chat.valid?
	end
	test "content should not exceed 320 characters" do
		@chat.content = "a" * 321
		assert_not @chat.valid?
	end
	test "chat order should be most recent first" do
		assert_equal chats(:most_recent), Chat.first
	end
end