require 'test_helper'
class ChatsControllerTest < ActionController::TestCase
	def setup
		@chat = chats(:orange)
	end
	test "accessing create when not signed in should redirect to sign in"  do
		assert_no_difference 'Chat.count' do
			post :create, chat: { content: "Lorem ipsum" }
		end
		assert_redirected_to sign_in_path
	end
	test "accessing destroy when not signed in should redirect to sign in" do
		assert_no_difference 'Chat.count' do
			post :destroy, id: @chat
		end
		assert_redirected_to sign_in_path
	end
	test "accessing destroy for another user post should redirect to root" do
		sign_in_as(users(:michael))
		chat = chats(:ants)
		assert_no_difference 'Chat.count' do
			delete :destroy, id: chat
		end
		assert_redirected_to root_path
	end
end