require 'test_helper'
class RelationshipsControllerTest < ActionController::TestCase
	test "create should require signed in user" do
		assert_no_difference 'Relationship.count' do
			post :create
		end
		assert_redirected_to sign_in_path
	end
	test "destroy should require signed in user" do
		assert_no_difference 'Relationship.count' do
			delete :destroy, id: relationships(:one)
		end
		assert_redirected_to sign_in_path
	end
end