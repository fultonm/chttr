require 'test_helper'
class UsersControllerTest < ActionController::TestCase
	def setup
		@user = users(:michael)
		@other_user = users(:archer)
	end
  test "accessing user index when not signed in should redirect to sign in" do
    get :index
    assert_redirected_to sign_in_path
  end
  test "should get new user" do
    get :new
    assert_response :success
  end
  test "accessing edit when not signed in should redirect to sign in" do
  	get :edit, id: @user
  	assert_not flash.empty?
  	assert_redirected_to sign_in_path
  end
  test "accessing update when not signed in should redirect to sign in" do
  	patch :update, id: @user, user: { name: @user.name, email: @user.email }
  	assert_not flash.empty?
  	assert_redirected_to sign_in_path
  end
	test "accessing edit of another user should redirect to root" do
    sign_in_as(@other_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_path
  end
  test "accessing update of another user should redirect to root" do
  	sign_in_as(@other_user)
  	patch :update, id: @user, user: { name: @user.name, email: @user.email }
  	assert flash.empty?
  	assert_redirected_to root_path
  end
  test "accessing destroy when not signed in should redirect to sign in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to sign_in_path
  end
  test "accessing destory when signed in as non admin should redirect to root" do
    sign_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_path
  end
  test "accessing following when not signed in should redirect to root" do
  	get :following, id: @user
  	assert_redirected_to sign_in_path
  end
  test "accessing followers when not signed in should redirect to root" do
  	get :followers, id: @user
  	assert_redirected_to sign_in_path
  end
end