require 'test_helper'
class UsersIndexTest < ActionDispatch::IntegrationTest
	def setup
		@user = @admin = users(:michael)
		@non_admin = users(:archer)
	end
	test "index should include pagination and active users only" do
		sign_in_as(@user)
		@non_admin.update_attribute(:activated, false)
		get users_path
		assert_template 'users/index'
		assert_select 'div.pagination'
		assert_no_match @non_admin.name, response.body
		User.where(activated: true).paginate(page: 1).each do |user|
			assert_select 'a[href=?]', user_path(user), text: user.name
		end
	end
	test "when signed in as admin index should include pagination and delete links and delete links function" do
		sign_in_as(@admin)
		get users_path
		assert_template 'users/index'
		assert_select 'div.pagination'
		first_page_of_users = User.paginate(page: 1)
		first_page_of_users.each do |user|
			assert_select 'a[href=?]', user_path(user), text: user.name
			unless user == @admin
				assert_select 'a[href=?]', user_path(user), text: 'delete'
			end
		end
		assert_difference 'User.count', -1 do
			delete user_path(@non_admin)
		end
	end
	test "when signed in as non-admin index should not include delete links" do
		sign_in_as(@non_admin)
		get users_path
		assert_select 'a', text: 'delete', count: 0
	end
end