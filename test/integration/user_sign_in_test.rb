require 'test_helper'
class UserSignInTest < ActionDispatch::IntegrationTest
	def setup
		@user = users(:michael)
	end
	test "sign in with invalid information should give error" do 
		get sign_in_path
		assert_template 'sessions/new'
		post sign_in_path, session: { email: "", password: ""}
		assert_template 'sessions/new'
		assert_not flash.empty?
		get root_path
		assert flash.empty?			
	end
	test "sign in with valid information followed by sign out should succeed" do
		get sign_in_path
		post sign_in_path, session: { email: @user.email, password: 'password' }
		assert is_signed_in?
		assert_redirected_to @user
		follow_redirect!
		assert_template 'users/show'
		assert_select "a[href=?]", sign_in_path, count: 0
		assert_select "a[href=?]", sign_out_path
		assert_select "a[href=?]", user_path(@user)
		delete sign_out_path
		assert_not is_signed_in?
		assert_redirected_to root_path
		delete sign_out_path
		follow_redirect!
		assert_select "a[href=?]", sign_in_path
    assert_select "a[href=?]", sign_out_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
	end
	test "signing in with remember_me should have cookie" do
		sign_in_as(@user, remember_me: '1')
		assert_not_nil cookies['remember_token']
	end
	test "signing in without remember_me should not have cookie" do
		sign_in_as(@user, remember_me: '0')
		assert_nil cookies['remember_token']
	end
end
