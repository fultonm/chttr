require 'test_helper'

class ApiTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    sign_in_as(@user)
  end
  test "relationship_count" do
    get '/api/relationships'
    response_hash = JSON.parse(response.body)
    assert_equal @user.followers.count, response_hash["followers"].count
    assert_equal @user.following.count, response_hash["following"].count
  end
end
