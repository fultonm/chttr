require 'test_helper'
class UsersSignUpTest < ActionDispatch::IntegrationTest
	def setup
		ActionMailer::Base.deliveries.clear
	end
	test "invalid sign up information should not save" do
		assert_no_difference 'User.count' do
			post users_path, user: { name: "", 
									 email: "user@invalid",
									 password: "foobar",
									 password_confirmation: "foobaz" }
		end
		assert_template 'users/new'
	end
	test "valid sign up information with account validation" do
		get sign_up_path
		assert_difference 'User.count', 1 do
			post users_path, user: { name: "Exam Peel",
												  		 email: "example@example.com",
												  		 password: "validpw",
												  		 password_confirmation: "validpw" }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    sign_in_as(user)
    assert_not is_signed_in?
    get edit_account_activation_path("invalid token")
    assert_not is_signed_in?
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_signed_in?
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_signed_in?
	end	
end