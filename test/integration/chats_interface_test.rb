require 'test_helper'
class ChatsInterfaceTest < ActionDispatch::IntegrationTest
	def setup
		@user = users(:michael)
	end
	test "chat interface should display correct information" do
		sign_in_as(@user)
		get root_path
		assert_select 'div.pagination'
		assert_no_difference 'Chat.count' do
			post chats_path, chat: { content: "" }
		end
		assert_select 'div#error_explanation'
		content = "This content has the power to validate a post"
		assert_difference 'Chat.count', 1 do
			post chats_path, chat: { content: content }
		end
		assert_redirected_to root_path
		follow_redirect!
		assert_match content, response.body
		assert_select 'a', text: 'delete'
		first_chat = @user.chats.paginate(page: 1).first
		assert_difference 'Chat.count', -1 do
			delete chat_path(first_chat)
		end
		get user_path(users(:archer))
		assert_select 'a', text: 'delete', count: 0
	end
	test "chat sidebar count should have correct count" do
		sign_in_as(@user)
		get root_path
		assert_match "#{@user.chats.count} chats", response.body
		# User with 0 chats
		other_user = users(:mallory)
		sign_in_as(other_user)
		get root_path
		assert_match "0 chats", response.body
		other_user.chats.create!(content: "A chat")
		get root_path
		assert_match "1 chat", response.body
	end
end